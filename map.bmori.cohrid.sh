#!/bin/bash
#SBATCH --job-name=map.bmori.cohrid

#SBATCH --output=map.bmori.cohrid_output

#SBATCH -c 30

#

#SBATCH --time=96:00:00

#SBATCH --mem=10G

#SBATCH --mail-user=luisa.scolari@ist.ac.at

#SBATCH --mail-type=ALL

#

#SBATCH --no-requeue

#

#SBATCH --export=NONE

unset SLURM_EXPORT_ENV

#

#Set the number of threads to the SLURM internal variable
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#

module load blat

srun blat -q=dnax -t=dnax -minScore=50 Cameraria_ohridella_v1_scaffolds.fasta bombyx.cds.longestCDS bmori_CDS_vs_cohridGenome.blat
