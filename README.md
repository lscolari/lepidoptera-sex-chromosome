# Lepidoptera Sex Chromosome Assignment
Part 1. The goal is to assign scaffolds of a genomic assembly to the sex chromosome (Z) or autosomes, based on coverage of both female and male reads of the species. Scaffolds were assigned to the Z-chromosome when their female coverage were the half of its male coverage.

Part 2. In this case, we assign Cameraria ohridella scaffolds/contigs and compare it with Bombyx mori annotated genes.

Part 3. Scaffolds assigned as Z are cross-checked for corespondence with B. mori homologous chromosomes. If corresponding to Z AND an autosome, indication of neo-Z chromosome in the species. Then check via gene location if it is true. In this case, this was done for another Lepidoptera (Pieris rapae).

# Part 1. Assign scaffolds to Z chromosome or Autosome

## Download data (genome assembly + reads)
Copy genomic assembly from species (.zip) from Chris' folder to mine in the cluster:

```
#copy arhive from "chris_folder" to "my_folder_where_I_am_logged_now" (= ".")
cp  /archive3/group/vicosgrp/temp_for_luisa/Vicoso_Cohridella_Ndegeerella_Tsylvina_genome_assemblies.zip .
```
  
then unzip the copied file, which contains 3 fasta files (one for each species). One of them is the assembly from Cameraria ohridella.

```
unzip archive_name.zip
```

Download Bombyx mori genome assembly (Lepidoptera reference) from NCBI and decompress it:

```
#download B. mori genome
wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/014/905/235/GCF_014905235.1_Bmori_2016v1.0/GCF_014905235.1_Bmori_2016v1.0_genomic.fna.gz

#decompress it 
gzip -d GCF_014905235.1_Bmori_2016v1.0_genomic.fna.gz
``` 

Download Cameraria ohridella female and male reads from NCBI database - SRA (Sequence Read Archive) toolkit. Use a SLURM script for that.

```
#open a text editor
pico cohridella.sra.sh 

#!/bin/bash 
# 
#SBATCH --job-name=cohridella.sra.sh 
#SBATCH --output=cohridella.sra.sh_output 
# 
#SBATCH -c 5 
# 
#SBATCH --time=96:00:00 
# 
#SBATCH --mem=10G 
# 
#SBATCH --mail-user=luisa.scolari@ist.ac.at 
#SBATCH --mail-type=ALL 
# 
#SBATCH --no-requeue 
# 
#SBATCH --export=NONE 
unset SLURM_EXPORT_ENV 
# 
#Set the number of threads to the SLURM internal variable 
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK 

#DOWNLOAD THE PROGRAM USED TO READ THE GENOME DATA FROM THE SOURCE (NCBI) 
module load SRA-Toolkit/2.8.1-3 

#SPLIT FQ1 and FQ2 FEMALE DNA READS 
srun fastq-dump --skip-technical --readids --split-files SRR5626452 
#SPLIT FQ1 and FQ2 MALE DNA READS 
srun fastq-dump --skip-technical --readids --split-files SRR5626454 

^X 
Y 
Y 

#submit it into the cluester queue
sbatch cohridella.sra.sh 
```

Verify the job status:

```
squeue -u my_username
#ST is “state” → R is “running”, P is “pending” (waiting in the queue) 
```

Outputs are reverse & forward, female & male reads: SRR5626452_1.fastq & _2.fastq; SRR5626454_1 & _2.fastq 

## Map C. ohridella reads to genome 
Download and run bowtie2 in order to map the SRA reads to the genomic scaffolds. First, index the genome (C. ohridella scaffolds) with it:

```
pico cohridella.index.sh

#!/bin/bash 
# 
#SBATCH --job-name=cohridella.index 
#SBATCH --output=cohridella.index _output 
# 
#SBATCH -c 30 
# 
#SBATCH --time=96:00:00 
# 
#SBATCH --mem=10G 
# 
#SBATCH --mail-user=luisa.scolari@ist.ac.at 
#SBATCH --mail-type=ALL 
# 
#SBATCH --no-requeue 
# 
#SBATCH --export=NONE 
unset SLURM_EXPORT_ENV 
# 
#Set the number of threads to the SLURM internal variable 
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK 
# 

#download bowtie2 
module load  bowtie2/2.3.4.1  

#index the genome: "scaffolds_file" "name_the_indexed_genome"
srun bowtie2-build Cameraria_ohridella_v1_scaffolds.fasta CamohGenome 

^X 
Y 
“enter” 

sbatch cohridella.index.sh 
```

Outputs are: CamohGenome.1.bt2; .2.bt2; .3.bt2; .4.bt2; .rev.1.bt2; .rev.2.bt2.

Now use bowtie2 to map both the Female and Male reads to the indexed genome of C. ohridella ("CamohGenome"):

```
pico cohridella.map.sh 

#!/bin/bash 
# 
#SBATCH --job-name=cohridella.map 
#SBATCH --output=cohridella.map_output 
# 
#SBATCH -c 30 
# 
#SBATCH --time=96:00:00 
# 
#SBATCH --mem=10G 
# 
#SBATCH --mail-user=luisa.scolari@ist.ac.at 
#SBATCH --mail-type=ALL 
# 
#SBATCH --no-requeue 
# 
#SBATCH --export=NONE 
unset SLURM_EXPORT_ENV 
# 
#Set the number of threads to the SLURM internal variable 
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK 
# 

#download bowtie2 
module load  bowtie2/2.3.4.1 

#run bowtie2 to map both female and male reads to the genome
#female
srun bowtie2 -x CamohGenome -1 SRR5626452_1.fastq -2 SRR5626452_2.fastq --end-to-end --sensitive -p 50 -S Female.sam 
#male
srun bowtie2 -x CamohGenome -1 SRR5626454_1.fastq -2 SRR5626454_2.fastq --end-to-end --sensitive -p 50 -S Male.sam 

^X 
Y 
Y 

sbatch cohridella.map.sh 
```

Outputs are: Female.sam, Male.sam (contains association of Scaffold name and reads).
Check in the _output file if the alignment rate is >70%. 

## Estimate coverage 
Use SOAP.cov to estimate coverage (average of number of matching reads) of both male and female reads in each scaffold.

```
pico cohridella.coverage.sh 

#!/bin/bash 
# 
#SBATCH --job-name=cohridella.coverage 
#SBATCH --output=cohridella.coverage_output 
# 
#SBATCH -c 30 
# 
#SBATCH --time=96:00:00 
# 
#SBATCH --mem=10G 
# 
#SBATCH --mail-user=luisa.scolari@ist.ac.at 
#SBATCH --mail-type=ALL 
# 
#SBATCH --no-requeue 
# 
#SBATCH --export=NONE 
unset SLURM_EXPORT_ENV 
# 
#Set the number of threads to the SLURM internal variable 
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK 
# 

#download the module 
module load soap 

#run SOAPcov (Version: 2.7.7) 
srun soap.coverage -sam -cvg -i Female.sam -onlyuniq -p 50 -refsingle Cameraria_ohridella_v1_scaffolds.fasta -o Female.soapcov 

srun soap.coverage -sam -cvg -i Male.sam -onlyuniq -p 50 -refsingle Cameraria_ohridella_v1_scaffolds.fasta -o Male.soapcov 

^X 
Y 
Y 

sbatch cohridella.coverage.sh 
```

Outputs are: Female.soapcov & Male.soapcov (contains the Sacaffold name, % of matching reads to it, and its "Depth" = average number of matching reads = coverage).

Both the .soapcov tables now need to be clean and sorted, for them to contain only the scaffold name, its length and coverage in alphabetical order.

```
cat Female.soapcov | awk '{print $1, $2, $4}' | perl -pi -e 's/:.*\// /gi' | perl -pi -e 's/Depth://gi' | sort > Female.soapfinal 

 
cat Male.soapcov | awk '{print $1, $2, $4}' | perl -pi -e 's/:.*\// /gi' | perl -pi -e 's/Depth://gi' | sort > Male.soapfinal 
```

Outputs are: Female.soapfinal & Male.soapfinal, which contain only scaffold name, its length and coverage.

Now join both the Female and Male tables into one (this is why both of them needed to be sorted alphabetically, so we can match F and M info).

```
join Female.soapfinal Male.soapfinal | perl -pi -e 's/^\n.*//gi' > FemaleMale.soapfinal 
```

Output is: FemaleMale.soapfinal, which contains scaffold name, length for the Female reads, Female coverage, length for the Male reads, Male coverage.

This final coverage table needs to be copied from the Cluster to the local computer, so it can be worked in R for the chromosome assignment. For that, a new terminal window needs to be opened:

```
#print the location you are working on the cluster
pwd

#scp cluster_location computer_location
scp lscolari@bea81:/nfs/scistore03/vicosgrp/lscolari/lepidoptera-sex-chromosome/FemaleMale.soapfinal ~/Documents/sex_chromosome_project 
```

Remove the sam files from the cluster folder, as it uses a lot of space.

```
rm Female.sam

rm Male.sam
```

## Assign Scaffolds to Chromosome (Z or Autosome)
Open the FemaleMale.soapfinal file manually from the computer location to which it was copied to. Delete manually its last lines that does not correspond to the desired information (scaffold name, length, coverage).

Go to R-studio to assign each scaffold a chromosome (Z or Autosome).

```
#PLOT COVERAGE DENSITY (log2(F/M)) 

#load and format coverage tables 

#tell which directory to work in 
setwd("~/Documents/sex_chromosome_project/cohridella") 

#load the coverage file (.soapfinal) 
cov <- read.table("FemaleMale.soapfinal", head=F, sep=" ") 

#name the columns of the table 
colnames(cov)<-c("Scaffold", "F_length", "Fcov", "M_length", "Mcov") 

#check if the name looks good 
head(cov)

#calculate log2 of Fcov/Mcov in the table "cov" and call it “ratio” 
cov$ratio<-log2(cov$Fcov/cov$Mcov) 

#check if the calculation worked 
head(cov) 

#PLOT DENSITY = distribution of log2(Fcov/Mcov) 
#Plot the density of log2(Fcov/Mcov). Remember, coverage is the average count of reads that match a scaffold. If both Female and Male reads mapped similarly to a certain scaffold, Fcov/Mcov should be around 1. Thus, log2(1) = 0. 
#In Lepidoptera, Famales have WZ and Males ZZ sex chromosomes. So for the scaffold corresponding to the Z-chromosome, Males should have about twice as many reads mapping to them as Females. Thus, Fcov/(2.Fcov) = 1/2. Thus, log2(1/2) = -1.
#So we plot this ratio to visualize and thereafter be able to sort scaffolds with density values around 0 (autosomes), and the ones around -1 (Z-chromosome).

#create a pdf file to plot the density of the reads ratio 
pdf("Density.pdf", width=8, height=8) 

#plot the density of the ratio of F:M coverage (=distribution of log2(Fcov/Mcov)) 
par(mfrow=c(1,1)) 

plot(density(cov$ratio, adjust=2, na.rm = T), xlim=c(-2,2), main="", xlab="Log2(Fcov/Mcov)", cex.axis=1.5, cex.lab=1.5) 
print(density(cov$ratio, adjust=2, na.rm = T))

#add vertical line with the median of the F/M coverage (=autosomal peak) 
abline(v=median(cov$ratio, na.rm=T), col="red") 

#add vertical line with the (median of the F/M coverage) - 1 (=Z chromosome peak) 
abline(v=median(cov$ratio, na.rm=T)-1, col="red") 

#add a dotted vertical line to correspond to the split between autosomal and Z chromosomes 
abline(v=median(cov$ratio, na.rm=T)-0.5, col="red", lty=2) 

#tell R pdf is ready (close it) 
dev.off() 

#ASSIGN SCAFFOLDS TO AUTOSOME OR Z 

#select Z Scaffolds from the distribution plot (= ratio<-0.5), in a subset called “zchr”. 
zchr<-subset(cov, ratio<(median(cov$ratio, na.rm=T)-0.5)) 

#add Z to a new column ("chromosome") in the table “zchr”. 
zchr$chromosome<-"Z" 

#check if that worked 
head(zchr) 

#select autosomal Scaffolds from the distribution plot (= ratio>-0.5), in a subset called “achr”. 
achr<-subset(cov, ratio>(median(cov$ratio, na.rm=T)-0.5)) 

#add "A" to a new column ("chromosome") in the table achr 
achr$chromosome<-"A" 

#check if that worked 
head(achr) 

#combine both zchr and achr into a new table, and assign this combined table as the final one. Write it into text file 
finalcov<-rbind(zchr, achr) 

write.table(finalcov, file="finalcov.txt", row.names=F, quote=F) 

#the text file (finalcov.txt table) can be opened in the computer and it is the “zchr” table stacked on top of the “achr” table 

#output: finalcov.txt (scaffold – assigned chromosome) 
```

"finalcov.txt" can be opened in the computer and it is the “zchr” table stacked on top of the “achr” table, which contains: scaffold - assigned chromosome.

# Part 2. Check assignment comparing to Bombyx mori annotations

Compare the Z or A assignment with a "correct" chromosome assignment from annotaded genes. In the case of C. ohridella, Bombyx mori can be used as a reference.

## Get Bomby mori data

Download B. mori annotated genes, coding sequence file (CDS), in this case from NCBI (https://www.ncbi.nlm.nih.gov/genome/76).
Note for the future: the Z chromosome for B. mori is the chromosome #1 = NC_051358.1

```
wget https://ftp.ncbi.nlm.nih.gov/genomes/refseq/invertebrate/Bombyx_mori/latest_assembly_versions/GCF_014905235.1_Bmori_2016v1.0/GCF_014905235.1_Bmori_2016v1.0_cds_from_genomic.fna.gz 

#unzip it
gzip -d GCF_014905235.1_Bmori_2016v1.0_cds_from_genomic.fna.gz 
```

This document contains (amongst other things): chromosome name, gene name, gene sequence.

## Get 'gene name' - 'gene sequence' correspondence
### Get the file with the longest CDS
There are multiple CDS for the same gene, as genes are: regulatory sequence + introns + exons. CDS are the combination of regulatory sequences + introns (no exons), so the same gene can result in several CDS.
Because of that, we want to get only 1 CDS per gene (it is enough), so we will look for the  longest CDS per gene.

First, check if there are indeed several sequences for the same gene. With perl, use “grep” to search the “>” in the fasta file (lines begin with it). Then cleaned it before “gene” and after “]”, sorted. Count how many times each particle (in this case, the name of the gene) appears. If >1, it means there are several CDS for a gene. Select the ones that >1, use wc (word count) to check how many. It is a lot. So it needs to be cleaned.... 

Clean and sort CDS fasta file for it to contain just: gene - sequence, so it could be used in the "getlongestCDS_v2" program, stored in Beatriz folder.

```
cat GCF_014905235.1_Bmori_2016v1.0_cds_from_genomic.fna | perl -pi -e 's/.*gene=/>/gi' | perl -pi -e 's/] .*//gi' | perl -pi -e 's/\n/ /gi' | perl -pip -e 's/>/\n>/gi' | sort | perl -pi -e 's/ /\n/gi' | perl -pi -e 's/^\n//gi' > bombyx.cds
```

This perl command line turns the fasta file into a table, and then back to a fasta file. Breaking it down, so we could addapt if necessary for other cds files:

```
#Substitute until “gene=” with “>”
perl -pi -e 's/.*gene=/>/gi' 

#Substitute “] ” onwards with a space “ “ (=delete)
perl -pi -e 's/] .*//gi' 

#Put an “enter” before “>” (‘n’ command, new line)
perl -pi -e 's/\n/ /gi' | perl -pip -e 's/>/\n>/gi' 

#We have a table.
#Sort it (by gene name). 
sort 

#Substitute “ “ (=empty space) with the next line
perl -pi -e 's/ /\n/gi' 

#If there is something starting with an empty space, get rid of it (‘^' command) (=with this we get rid of the 1st empty line)
perl -pi -e 's/^\n//gi' 

#g: globally 
#i: ignore charcter 
```

This is output into a file called "bombyx.cds', which contains: gene - sequence.

Run Beatriz' program to get the longest CDS:

```
perl /nfs/scistore03/vicosgrp/bvicoso/scripts/getlongestCDS_v2.pl bombyx.cds 
```

The output is bombyx.cds.longestCDS, which contains: gene - sequence (but now each gene appears just once).

## Get Bombyx mori gene locations (chromosomes)

Get 'gene' - 'true chromosome' correspondence.
This information (in which chromosome each gene is) is already in the cds fasta file, it just needs to be cleaned with some perl:

```
cat GCF_014905235.1_Bmori_2016v1.0_cds_from_genomic.fna | grep '>' | perl -pi -e 's/.*>lcl\|/ /gi' | perl -pi -e 's/_cds.*gene=/ /gi' | perl -pi -e 's/] .*//gi' | awk '{print $2, $1}' | sort | uniq > bombyx.chromosome 
```

The output is: bombyx.chromosome, which contains gene - true chrosomosome.

For future reference, breaking perl commands down:

```
#Look in the fasta for lines starting with “>” symbol. 
grep '>' 

#Substitute until “NC_” with “ ” (=space). The “\” sign indicates that the following sign is a chacter, not a command (in this case the “|”). 
perl -pi -e 's/.*>lcl\|/ /gi' 

#Substitute “_cds” onwards until “gene=” with a space " " (=delete). 
perl -pi -e 's/_cds.*gene=/ /gi' 

#Substitute “] ” onwards with a space " ". 
perl -pi -e 's/] .*/ /gi' 

#Write the table as column 2 (gene) and then column 1 (chromosome) (=flip them). 
awk '{print $2, $1}' 

#sort
sort

#colapse all genes that appeare more than once into just one
uniq
``` 

## Get 'B. mori gene' - 'C. ohridella scaffold' correspondence
### Map B. mori gene sequence to C. ohridella genomic assembly (scaffolds). 

For that, use blat, and input the B. mori longest cds file (bombyx.cds.longestCDS) and C. ohridella initial genomic assembly (Cameraria_ohridella_v1_scaffolds.fasta).

```
#!/bin/bash  
#SBATCH --job-name=map.bmori.cohrid 
#SBATCH --output=map.bmori.cohrid_output  
#SBATCH -c 30  
#  
#SBATCH --time=96:00:00  
#SBATCH --mem=10G  
#SBATCH --mail-user=luisa.scolari@ist.ac.at  
#SBATCH --mail-type=ALL  
#  
#SBATCH --no-requeue  
#  
#SBATCH --export=NONE  
unset SLURM_EXPORT_ENV  
#  
#Set the number of threads to the SLURM internal variable  
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK  
#  
module load blat 

srun blat Cameraria_ohridella_v1_scaffolds.fasta bombyx.cds.longestCDS bmori_CDS_vs_cohridGenome.blat   

^X
Y

#submit
sbatch map.bmori.cohrid.sh 
```

The output is bmori_CDS_vs_cohridGenome.blat, which contains (amongst other things): gene name (column Q name) - scaffold (column Q end).

### Take each gene only once (the one best mapping).

Take the best mapping gene location by its sequence, in order to pick each gene only once. For that, first sort the 'gene - scaffold' table (bmori_CDS_vs_cohridGenome.blat) by scaffold name (column #10):

```
sort -k 10 bmori_CDS_vs_cohridGenome.blat > bmori_CDS_vs_cohridGenome.sorted
```

Then take just the best hit for each gene (run Beatriz' program "besthitblat"):

```
perl /nfs/scistore03/vicosgrp/bvicoso//scripts/2-besthitblat.pl bmori_CDS_vs_cohridGenome.sorted
```

The output is: bmori_CDS_vs_cohridGenome.sorted.besthit, which contains (amongst other things): gene - scaffold.

Clean it in order to just have the gene and scaffold information, then sort it by gene name:

```
#select just columns #10 (gene) and #14 (scaffold)
cat bmori_CDS_vs_cohridGenome.sorted.besthit | cut -f 10,14 | sort > bmoriCDS_cohridScaffold.txt 
```

The output is: bmoriCDS_cohridScaffold.txt, which contains just gene - scaffold.

## Merge the 3 tables into one (gene - scaffold, gene - true chromosome, scaffold - assigned chromosome) 

Those tables are:
- B. mori gene - C. ohridella scaffold: bmoriCDS_cohridScaffold.txt
- B. mori gene - B. mori chromosome: bombyx.chromosome
- C. ohridella scaffold - assigned chromosome: finalcov.sorted

First, copy the "finalcov.txt" table (assigment made in R) from the computer to the cluster (in a new terminal window) and sort it by scaffold name:

```
scp ~/Documents/sex_chromosome_project/finalcov.txt lscolari@bea81:/nfs/scistore03/vicosgrp/lscolari/lepidoptera-sex-chromosome 

#sort it by scaffold name
sort finalcov.txt > finalcov.sorted 
```

And the, join the 3 tables:

```
sort bmoriCDS_cohridScaffold.txt | join /dev/stdin bombyx.chromosome | awk '{print $2, $1, $3}' | sort | join /dev/stdin finalcov.sorted > finalmatch.txt
```

Output: finalmatch.txt, which contains: C. ohridella scaffold - B. mori gene - B. mori true chromosome - lengths - coverage - C. ohridella assigned chromosome (Z or A).

## Plot comparison (assignment vs. B. mori chromosome) to check if the assignmet was correct

First, move the finalmatch.txt file from the cluster to the computer, to be able to work in R. Do it in a new terminal window.

```
scp lscolari@bea81:/nfs/scistore03/vicosgrp/lscolari/lepidoptera-sex-chromosome/finalmatch.txt ~/Documents/sex_chromosome_project 
```

Go to R to plot the comparison: species assigned chromosome (Z or A) - B. mori "real" chromosome.

```
#Analyze how good the chromosome assignment for C. ohridella (Z-A) was vs. the correct one for B. mori

#select the directory to work on
setwd("~/Documents/sex_chromosome_project/cohridella") 

#load the final match table, which contains: scaffold – gene – chromosome - (length and coverage, Female and Male) – chromosome assignment  
cov<-read.table("finalmatch.txt", head=F, sep=" ")

#name the different columns so we know what they are 
colnames(cov)<-c("Scaffold", "Gene", "Chrom", "F_length", "Fcov", "M_length", "Mcov", "ratio","AssignedChromZA") 

#check if it looks good
head(cov)

#delete first row, if needed.
cov=cov[-1,]

#clear table to appear just true B. mori chromosome names (NC..., not NW....)
cov2 <- subset(cov, Chrom=="NC_051358.1" | Chrom=="NC_051359.1" | Chrom=="NC_051360.1" | Chrom=="NC_051361.1" | Chrom=="NC_051362.1" | Chrom=="NC_051363.1" | Chrom=="NC_051364.1" | Chrom=="NC_051365.1" | Chrom=="NC_051366.1" | Chrom=="NC_051367.1" | Chrom=="NC_051368.1" | Chrom=="NC_051369.1" | Chrom=="NC_051370.1" | Chrom=="NC_051371.1" | Chrom=="NC_051372.1" | Chrom=="NC_051373.1" | Chrom=="NC_051374.1" | Chrom=="NC_051375.1" | Chrom=="NC_051376.1" | Chrom=="NC_051377.1" | Chrom=="NC_051378.1" | Chrom=="NC_051379.1" | Chrom=="NC_051380.1" | Chrom=="NC_051381.1" | Chrom=="NC_051382.1" | Chrom=="NC_051383.1" | Chrom=="NC_051384.1" | Chrom=="NC_051385.1") 

head(cov2)

#replace ugly true chromosome names by their "numeric" name
ugly_names<-c("NC_051358.1", "NC_051359.1", "NC_051360.1", "NC_051361.1", "NC_051362.1", "NC_051363.1", "NC_051364.1", "NC_051365.1", "NC_051366.1", "NC_051367.1", "NC_051368.1", "NC_051369.1", "NC_051370.1", "NC_051371.1", "NC_051372.1", "NC_051373.1", "NC_051374.1", "NC_051375.1", "NC_051376.1", "NC_051377.1", "NC_051378.1", "NC_051379.1", "NC_051380.1", "NC_051381.1", "NC_051382.1", "NC_051383.1", "NC_051384.1", "NC_051385.1")

new_names<-c("chrZ", "chr02", "chr03", "chr04", "chr05", "chr06", "chr07", "chr08", "chr09", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chr23", "chr24", "chr25", "chr26", "chr27", "chr28")

#install.packages("mgsub")
library(mgsub)

cov2$Chrom=mgsub(cov2$Chrom, ugly_names, new_names)

head(cov2)

#arrange the table by Chromosome name
library(dplyr)
cov2<-arrange(cov2, Chrom)

head(cov2)

#download the ggplot2 library. If not installed yet, do so.
library(ggplot2)

#plot the frequency of each true B. mori chromosome that was assigned as Z for P. rapae
assignedZ <- ggplot() +
  geom_bar(aes(x=Chrom, color=Chrom), data=cov2[cov2$AssignedChromZA=="Z",])

assignedZ + ggtitle("Assigned Z") + scale_x_discrete(name="", breaks=NULL) + theme(legend.position="bottom")

#plot the frequency of each true B. mori chromosome that was assigned as A for P. rapae
assignedA <- ggplot() +
  geom_bar(aes(x=Chrom, color=Chrom), data=cov2[cov2$AssignedChromZA=="A",])

assignedA + ggtitle("Assigned A") + scale_x_discrete(name="", breaks=NULL) + theme(legend.position="bottom")

#select the genes that are assigned as Z for P. rapae, in a table gene - true chromosome
tableGene_allZ <- cov2[cov2$AssignedChromZA=="Z", 2:3]

head(tableGene_allZ)

write.table(tableGene_allZ, file="tableGene_allZ.txt", row.names=F, quote=F) 
```

You should have now 2 graphs: 
- Assigned Z (count of genes in each B. mori chromosomes that were assigned as Z), and
- Assigned A (count of genes in each B. mori chromosomes that were assigned as autosome). 

The expectation is that the Assigned Z one would peak in "chrZ" and, if it is a neo-Z, an autosome. The Assigned A should be the inverse.

Annother outcome is a table listing all the genes that were assigned as belonging to the Z chromosome. This one can be used to check if it is indeed a neo-Z.

# Part 3. Check if it is a neo-Z chromosome  (Z-autosome fusion)

if there is a NEO-Z, the scaffolds assigned as Z and belonging to the “true autosome” that corresponds to the 2nd higher frequency (other than the true Z) should correspond to a sequential chunk of the autosome. That means that the genes in the scaffolds from assigned Z & autosome should be sequential (closely located). 
This part of the pipeline is with Pieris rapae data, which were obtained analogously as the C. ohridella one.

## Find all gene starting locations in whole B. mori genome: gene – start location – chromosome  

Go to the directory B. mori genome is, and copy it and paste into P. rapae directory.

```
cp GCF_014905235.1_Bmori_2016v1.0_cds_from_genomic.fna /nfs/scistore03/vicosgrp/lscolari/lepidoptera-sex-chromosome/prapae-zchr 

#check the info we get from this file
head GCF_014905235.1_Bmori_2016v1.0_cds_from_genomic.fna.gz 
```

Cut the file in order to have just: gene – location – chromosome: 

```
cat GCF_014905235.1_Bmori_2016v1.0_cds_from_genomic.fna | grep '>' | perl -pi -e 's/.*>lcl\|/ /gi' | perl -pi -e 's/_cds.*gene=/ /gi' | perl -pi -e 's/] .*location=/ /gi' | perl -pi -e 's/] .*//gi' | awk '{print $2, $3, $1}' | sort | uniq > bombyx.genelocation 
```

These “join” and “complement” are because of the exons. 

select just the start location for each gene: gene – start – chromosome:

```
cat bombyx.genelocation | perl -pi -e 's/ .*complement\(/ /gi' | perl -pi -e 's/ .*join\(/ /gi' |  perl -pi -e 's/\.\..* / /gi' | sort | uniq > bombyx.genelocation.start 
```

## Get starting locations of P. rapae genes assigned as Z-chr: genes – start location – chromosome  

Copy tableGene_allZ.txt (gene – chromosome) to the cluster, on a new terminal window:

```
scp ~/Documents/sex_chromosome_project/prapae/tableGene_allZ.txt lscolari@bea81:/nfs/scistore03/vicosgrp/lscolari/lepidoptera-sex-chromosome/prapae-zchr 
```

Delete table’s 1st row (row’s name) and sort it by gene name:

```
tail -n +2 tableGene_allZ.txt > tableGene_allZ 

sort tableGene_allZ > tableGene_allZ.sorted 
```

Find genes in tableGene_allZ.sorted into bombyx.genelocation.start --> merge the 2 tables, via gene.

- bombyx.genelocation.start: gene – start location – B. mori chromosome  

- tableGene_allZ.sorted: gene (all assigned as P. rapae Z-chr) – ‘true chromosome’ 

```
sort bombyx.genelocation.start | join /dev/stdin tableGene_allZ.sorted | sort > bombyx.genelocation.prapaeZchr 
```

Output bombyx.genelocation.prapaeZchr: gene – start location - B. mori chromosome – ‘true chromosome’.

Copy both files from the cluster to the local computer, in a new terminal window.

```
scp lscolari@bea81:/nfs/scistore03/vicosgrp/lscolari/lepidoptera-sex-chromosome/prapae-zchr/bombyx.genelocation.start ~/Documents/sex_chromosome_project/prapae 

scp lscolari@bea81:/nfs/scistore03/vicosgrp/lscolari/lepidoptera-sex-chromosome/prapae-zchr/bombyx.genelocation.prapaeZchr ~/Documents/sex_chromosome_project/prapae 
```

## Plot the gene locations of both species to compare them

Go to R to plot the gene locations of both P. rapae "assigned as Z genes" and B. mori genes, in order to compare them.
If the P. rapae assigned-Z genes correspond to a sequential part of a B. mori autosome, most likely its Z chromosome evolved by fusion (neo-Z).

```
#plot the locations of both B. mori genes and P. rapae assigned as Z ones, on the same plot, to see if they come from close locations.  
#One plot per chromosome. 

#tell which directory to work 
setwd("~/Documents/sex_chromosome_project/prapae") 

#load the table with all the locations 
loc<-read.table("bombyx.genelocation.start", head=F, sep=" ") 

#name the different columns so we know what they are 
colnames(loc)<-c("Gene", "Location", "Chrom")

#use "head" to check that the table looks good 
head(loc) 

#replace ugly true chromosome names by their "numeric" name
ugly_names<-c("NC_051358.1", "NC_051359.1", "NC_051360.1", "NC_051361.1", "NC_051362.1", "NC_051363.1", "NC_051364.1", "NC_051365.1", "NC_051366.1", "NC_051367.1", "NC_051368.1", "NC_051369.1", "NC_051370.1", "NC_051371.1", "NC_051372.1", "NC_051373.1", "NC_051374.1", "NC_051375.1", "NC_051376.1", "NC_051377.1", "NC_051378.1", "NC_051379.1", "NC_051380.1", "NC_051381.1", "NC_051382.1", "NC_051383.1", "NC_051384.1", "NC_051385.1")

new_names<-c("chrZ", "chr02", "chr03", "chr04", "chr05", "chr06", "chr07", "chr08", "chr09", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chr23", "chr24", "chr25", "chr26", "chr27", "chr28")

#install.packages("mgsub")
library(mgsub)

loc$Chrom=mgsub(loc$Chrom, ugly_names, new_names)

#load the table with the start locations of P. rapae chromosomes assigned as Z 
loc2<-read.table("bombyx.genelocation.prapaeZchr", head=F, sep=" ") 

#name the different columns so we know what they are 
colnames(loc2)<-c("Gene", "Location", "Chrom_longname", "Chrom") 

#use "head" to check that the table looks good 
head(loc2) 

#check which are the B. mori chromosomes that were most assigned as Z-chr in P. rapae
#alternative for simply checking the "assigned_Z" graph

tableZ<-sort(table(loc2$Chrom), decreasing=TRUE)
tableZ<-data.frame(tableZ)
head(tableZ)

#PLOT, for each most assigned chromosome, the gene start locations (x-coordinate) of both B. mori & P. rapae assigned Z-chr, each one in a different y-coordinate (1 & 2). 

#add a dummy column, which will be the y-coordinates  
loc2$dummy=rep(2,length(loc2$Location)) 
loc$dummy=rep(1,length(loc$Location)) 

#install.packages("ggplot2") 
library(ggplot2) 

#check if it is a number or a character 
typeof(loc$Location) 

#for plotting one chromosome, just change for its name under Chrom==" "

#chr Z
chrZ <- ggplot() + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color="P. rapae"), data=loc2[loc2$Chrom=="chrZ",]) + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color=Chrom), data=loc[loc$Chrom=="chrZ",])

chrZ + ggtitle("Z chromosome") + scale_x_continuous(name="chrom_location") + scale_y_continuous(name="", breaks=NULL, limits=c(1,2.5)) + theme(legend.position="bottom", legend.title=element_blank())

#chr02
chr02 <- ggplot() + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color="P. rapae"), data=loc2[loc2$Chrom=="chr02",]) + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color=Chrom), data=loc[loc$Chrom=="chr02",])

chr02 + ggtitle("chromosome 02") + scale_x_continuous(name="chrom_location") + scale_y_continuous(name="", breaks=NULL, limits=c(1,2.5)) + theme(legend.position="bottom", legend.title=element_blank())

#chr24
chr24 <- ggplot() + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color="P. rapae"), data=loc2[loc2$Chrom=="chr24",]) + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color=Chrom), data=loc[loc$Chrom=="chr24",])

chr24 + ggtitle("chromosome 24") + scale_x_continuous(name="chrom_location") + scale_y_continuous(name="", breaks=NULL, limits=c(1,2.5)) + theme(legend.position="bottom", legend.title=element_blank())

#chr16
chr16 <- ggplot() + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color="P. rapae"), data=loc2[loc2$Chrom=="chr16",]) + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color=Chrom), data=loc[loc$Chrom=="chr16",])

chr16 + ggtitle("chromosome 16") + scale_x_continuous(name="chrom_location") + scale_y_continuous(name="", breaks=NULL, limits=c(1,2.5)) + theme(legend.position="bottom", legend.title=element_blank())

#chr28
chr28 <- ggplot() + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color="P. rapae"), data=loc2[loc2$Chrom=="chr28",]) + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color=Chrom), data=loc[loc$Chrom=="chr28",])

chr28 + ggtitle("chromosome 28") + scale_x_continuous(name="chrom_location") + scale_y_continuous(name="", breaks=NULL, limits=c(1,2.5)) + theme(legend.position="bottom", legend.title=element_blank())

#ALL B. mori chromosomes = ugly, too much info

loc_all<-subset(loc, Chrom %in% new_names)
head(loc_all)

chr_all <- ggplot() +
  geom_point(aes(x=as.numeric(Location), y=dummy, color="P. rapae"), data=loc2) + 
  geom_point(aes(x=as.numeric(Location), y=dummy, color=Chrom), data=loc_all)
 
chr_all + ggtitle("all chromosomes") + scale_x_continuous(name="chrom_location") + scale_y_continuous(name="", breaks=NULL, limits=c(1,2.5)) + theme(legend.position="bottom", legend.title=element_blank())
```

You should have now one graph per chrosomome comparing the genes locations from both the P. rapae assigned as Z, and all B. mori.
If the P. rapae gene locations appear as a cluster, sequentially, probaly its Z chromosome evolved by fusion of the ancient Z and a chunk of this homologous B. mori autosome.
