#!/bin/bash 

# 

#SBATCH --job-name=cohridella.map

#SBATCH --output=cohridella.map_output

# 

#SBATCH -c 30 

# 

#SBATCH --time=96:00:00 

# 

#SBATCH --mem=10G 

# 

#SBATCH --mail-user=luisa.scolari@ist.ac.at

#SBATCH --mail-type=ALL 

# 

#SBATCH --no-requeue 

# 

#SBATCH --export=NONE 

unset SLURM_EXPORT_ENV 

# 

#Set the number of threads to the SLURM internal variable 

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK 

# 

 

#DOWNLOAD THE PROGRAM TO MAP (bowtie2)
module load  bowtie2/2.3.4.1

#RUN BOWTIE2 to map reads to genome
#female
srun bowtie2 -x CamohGenome -1 SRR5626452_1.fastq -2 SRR5626452_2.fastq --end-to-end --sensitive -p 50 -S Female.sam --no-unal

#male
srun bowtie2 -x CamohGenome -1 SRR5626454_1.fastq -2 SRR5626454_2.fastq --end-to-end --sensitive -p 50 -S Male.sam --no-unal
