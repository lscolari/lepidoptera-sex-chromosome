#!/bin/bash 

# 

 
 

#SBATCH --job-name=cohridella.sra.sh 

#SBATCH --output=cohridella.sra.sh_output 

# 

#SBATCH -c 5 

# 

#SBATCH --time=96:00:00 

# 

#SBATCH --mem=10G 

# 

#SBATCH --mail-user=luisa.scolari@ist.ac.at 

#SBATCH --mail-type=ALL 

# 

#SBATCH --no-requeue 

# 

#SBATCH --export=NONE 

unset SLURM_EXPORT_ENV 

# 

#Set the number of threads to the SLURM internal variable 

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK 

 

 

#DOWNLOAD THE PROGRAM USED TO READ THE GENOME DATA FROM THE SOURCE (NCBI) 

module load SRA-Toolkit/2.8.1-3 

 

#SPLIT FQ1 and FQ2 FEMALE DNA READS 

srun fastq-dump --skip-technical --readids --split-files SRR5626452

#SPLIT FQ1 and FQ2 MALE DNA READS 

srun fastq-dump --skip-technical --readids --split-files SRR5626454
